<?php
declare(strict_types = 1);

namespace App\Jobs;

use Carbon\Carbon;
use CSV\Model\CsvFile;
use CSV\Model\CsvRow;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class ProcessCsv
 * @package App\Jobs
 */
class ProcessCsv implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var CsvFile
     */
    private $csvFile;

    /**
     * Create a new job instance.
     *
     * @param CsvFile $csvFile
     */
    public function __construct(CsvFile $csvFile)
    {
        $this->csvFile = $csvFile;
    }

    /**
     * Possible split to different strategies for different header formats.
     *
     * @return void
     */
    public function handle(): void
    {
        $assoc = $this->getAssoc(storage_path() . '/app/' . $this->csvFile->getPath());
        $assocNoHeader = array_slice($assoc, 1);
        $chunks = collect($assocNoHeader)->chunk(1000);

        foreach ($chunks->toArray() as $chunk)
        {
            $chunk = $this->prepareChunk($chunk);
            CsvRow::query()->insert($chunk);
        }

        $this->csvFile->setProcessed(true);
        $this->csvFile->save();
    }

    /**
     * @param string $path
     * @return array
     */
    public function getAssoc(string $path): array
    {
        $csv = [];
        $lines = file($path, FILE_IGNORE_NEW_LINES);

        $explodedKeys = explode(',', $lines[0]);
        $keysCount = count($explodedKeys);

        foreach ($lines as $row) {
            $explodedRows = explode(',', $row);
            if (count($explodedRows) === $keysCount) {
                $csv[] = array_combine($explodedKeys, $explodedRows);
            }
        }

        return $csv;
    }

    /**
     * @param array $chunk
     * @return array
     */
    private function prepareChunk(array $chunk): array
    {
        foreach ($chunk as $key => $row) {
            $chunk[$key]['last_name'] = $row['lastname'];
            $chunk[$key]['first_name'] = $row['firstname'];
            $chunk[$key]['birth_timestamp'] = (int) strtotime($row['birthDate']);
            $chunk[$key]['created_at'] = Carbon::now('UTC')->getTimestamp();
            $chunk[$key]['csv_file_id'] = $this->csvFile->getId();

            unset($chunk[$key]['lastname'], $chunk[$key]['firstname'], $chunk[$key]['birthDate']);
        }

        return $chunk;
    }
}
