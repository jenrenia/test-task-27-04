<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCsvRowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('csv_rows', function (Blueprint $table) {
            $table->id();
            $table->string('category',64)->charset('utf8');
            $table->string('first_name',64)->charset('utf8');
            $table->string('last_name',64)->charset('utf8');
            $table->string('email',64)->charset('utf8');
            $table->string('gender',64)->charset('utf8');
            $table->integer('birth_timestamp')->unsigned();
            $table->integer('created_at')->unsigned();
            $table->integer('updated_at')->unsigned()->nullable()->default(null);
            $table->bigInteger('csv_file_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('csv_rows');
    }
}
