<table border="black">
    <tr>
        <td>Filename</td>
        <td>Unique id(Clickable for processed)Download</td>
        <td>Is processed</td>
        <td>Created at</td>
    </tr>
    @foreach( $files as $file )
        <tr>
            <td>{{$file->name}}</td>
            <td>@if($file->processed === 1) <a href="/documents/{{$file->unique_id}}">{{$file->unique_id}}</a>@else {{$file->unique_id}} @endif</td>
            <td bgcolor="@if($file->processed === 1) green @else red @endif">{{$file->processed}}</td>
            <td>{{\Carbon\Carbon::createFromTimestamp($file->created_at)->toDateTimeLocalString()}}</td>
        </tr>
    @endforeach
</table>
__________________________
<form action="/csv-file" method="post" enctype="multipart/form-data">
    <input type="file" name="file">
    <button type="submit">Send</button>
</form>
