<style>
    svg {
        width: 20px;
    }
</style>
<td><a href="/documents">Go back</a></td>
Preview of 20:(<a href="{{str_replace(Request::path(), Request::path() . '/download',Request::fullUrl())}}">Download filtered</a> )
<table border="black">
    <tr>
        <td>Category</td>
        <td>Last name</td>
        <td>First name</td>
        <td>Email</td>
        <td>Gender</td>
        <td>Birth date</td>
        <td>Age</td>
    </tr>
    @foreach( $rows as $row )
        <tr>
            <td>{{$row->category}}</td>
            <td>{{$row->first_name}}</td>
            <td>{{$row->last_name}}</td>
            <td>{{$row->email}}</td>
            <td>{{$row->gender}}</td>
            <td>{{\Carbon\Carbon::createFromTimestamp($row->birth_timestamp)->toDateTimeLocalString()}}</td>
            <td>{{\Carbon\Carbon::createFromTimestamp($row->birth_timestamp)->age}}</td>
        </tr>
    @endforeach
</table>

<br>
<br>

<form action="{{Request::url()}}" method="get">
    <label>
        Category:
        <select name="category">
            @foreach( $categories as $category )
                <option value="{{$category}}">{{$category}}</option>
            @endforeach
        </select>
    </label>

    <label>
        Gender:
        <select name="gender">
            @foreach( $genders as $gender )
                <option value="{{$gender}}">{{$gender}}</option>
            @endforeach
        </select>
    </label>

    <label>
        Date from:
        <input name="birth_date_lower" value="{{\Carbon\Carbon::createFromTimestamp($lowestDate->birth_timestamp)->isoFormat('YYYY-MM-DDTHH:MM:SS')}}" type="datetime-local">
    </label>

    <label>
        Date to:
        <input name="birth_date_upper" value="{{\Carbon\Carbon::createFromTimestamp($highestDate->birth_timestamp)->isoFormat('YYYY-MM-DDTHH:MM:SS')}}" type="datetime-local">
    </label>

    <button type="submit">Search</button>
</form>

<br>
<br>
{{ $rows->appends($_GET)->links() }}
