<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

## To run execute following:

- composer install
- ./vendor/bin/sail up
- ./vendor/bin/sail artisan migrate
- open in browser URL: localhost/documents


Keep running: exec artisan queue:work database --queue=default --timeout=600 --tries=3
