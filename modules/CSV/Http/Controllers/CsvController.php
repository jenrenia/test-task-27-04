<?php
declare(strict_types = 1);

namespace CSV\Http\Controllers;

use Carbon\Carbon;
use CSV\Model\CsvFile;
use CSV\Model\CsvRow;
use CSV\Service\CsvService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Config;
use Psy\VersionUpdater\Downloader\FileDownloader;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Class CsvController
 *
 * @package CSV\Http\Controllers
 */
class CsvController extends BaseController
{
    /**
     * @var CsvService
     */
    private $csvService;

    /**
     * CsvController constructor.
     *
     * @param CsvService $csvService
     */
    public function __construct(CsvService $csvService)
    {
        $this->csvService = $csvService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function upload(Request $request): JsonResponse
    {
        //file format check
        $file = $request->file('file');

        //fields validation

        if (null !== $file) {
            $this->csvService->processFile($file);
        }

        return new JsonResponse(['status' => 'job queued']);
    }

    /**
     * Should be probably moved to the "Export worker" too
     *
     * @param Request $request
     * @return StreamedResponse
     */
    public function download(Request $request)
    {
        /** @var CsvFile $csvFile */
        $csvFile = CsvFile::query()
            ->where('unique_id', $request->documentUniqueId)
            ->first();

        $rows = $this->csvService->getRows(
            $csvFile->getId(),
            $request->gender,
            $request->category,
            $request->birth_date_lower,
            $request->birth_date_higher,
        )
        ->get();

        //should be encapsulated in a response factory

        $headers = [
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
            ,   'Content-type'        => 'text/csv'
            ,   'Content-Disposition' => 'attachment; filename=galleries.csv'
            ,   'Expires'             => '0'
            ,   'Pragma'              => 'public'
        ];

        $callback = function() use ($rows)
        {
            $FH = fopen('php://output', 'w');
            foreach ($rows as $row) {
                fputcsv($FH, $row->toArray());
            }
            fclose($FH);
        };

        return response()->stream($callback, 200, $headers);
    }

    /**
     * It should not be here at all, if we building a back-end RESTfull API, no "views" allowed. Just to demonstrate how filters work.
     *
     * @return Application|Factory|View
     */
    public function showDocsList()
    {
        //should be in a repo, here we inject service, and repo in a service, but for speed like this.
        $files = CsvFile::query()
            ->orderByDesc('created_at')
            ->get();

        return view('showDocuments', compact('files') );
    }

    /**
     * It should not be here at all, if we building a back-end RESTfull API, no "views" allowed. Just to demonstrate how filters work.
     *
     * @param Request $request
     * @return Application|Factory|View
     */
    public function showDocContent(Request $request)
    {
        //query string validation should be here

        //should be found via object mapper then in a service via repo, but that's time to refactor
        /** @var CsvFile $csvFile */
        $csvFile = CsvFile::query()
            ->where('unique_id', $request->documentUniqueId)
            ->first();

        $rows = $this->csvService->getRows(
            $csvFile->getId(),
            $request->gender,
            $request->category,
            $request->birth_date_lower,
            $request->birth_date_higher,
        )
            ->paginate(Config::get('view.paginate_by', 10));

        //of course not in a controller, but no time
        $categories = CsvRow::query()
            ->select('category')
            ->distinct()
            ->get()
            ->pluck('category');

        $genders = CsvRow::query()
            ->select('gender')
            ->distinct()
            ->get()
            ->pluck('gender');

        $lowestDate = CsvRow::query()
            ->select('birth_timestamp')
            ->orderBy('birth_timestamp')
            ->first();

        $highestDate = CsvRow::query()
            ->select('birth_timestamp')
            ->orderByDesc('birth_timestamp')
            ->first();

        return view('showDocument', compact('rows', 'categories', 'genders', 'lowestDate', 'highestDate') );
    }
}
