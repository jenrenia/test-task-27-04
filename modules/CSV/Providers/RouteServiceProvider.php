<?php
namespace CSV\Providers;

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

use Illuminate\Routing\Router;

/**
 * Class RouteServiceProvider
 *
 * @package CSV\Providers
 */
class RouteServiceProvider extends ServiceProvider
{
    private const UNIQUE_ID_PATTERN = '[a-z0-9]{40}';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     * @throws BindingResolutionException
     */
    public function boot(): void
    {
        /**
         * @var Router $router
         * */
        $router = $this->app->make('router');
        $router->namespace('CSV\Http\Controllers')
            ->group(function () use ($router) {
                $this->mapRoutes($router);
            });
    }

    /**
     * @param Router $router
     */
    protected function mapRoutes(Router $router): void
    {
        /** @uses \CSV\Http\Controllers\CsvController::upload() */
        $router->post('/csv-file', 'CsvController@upload');

        /** @uses \CSV\Http\Controllers\CsvController::showDocsList() */
        $router->get('/documents', 'CsvController@showDocsList');

        /** @uses \CSV\Http\Controllers\CsvController::showDocContent() */
        $router
            ->get('/documents/{documentUniqueId}', 'CsvController@showDocContent')
            ->where('documentUniqueId', self::UNIQUE_ID_PATTERN);

        /** @uses \CSV\Http\Controllers\CsvController::download() */
        $router
            ->get('/documents/{documentUniqueId}/download', 'CsvController@download')
            ->where('documentUniqueId', self::UNIQUE_ID_PATTERN);
    }
}
