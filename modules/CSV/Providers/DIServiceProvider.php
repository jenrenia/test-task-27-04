<?php
declare(strict_types = 1);

namespace CSV\Providers;

use CSV\Service\CsvService;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

/**
 * Class DIServiceProvider
 * @package CSV\Providers
 */
class DIServiceProvider extends BaseServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->singleton(CsvService::class, function () {
            return new CsvService(123);
        });
    }
}
