<?php
declare(strict_types = 1);

namespace CSV\Service;

use App\Jobs\ProcessCsv;
use Carbon\Carbon;
use CSV\Model\CsvFile;
use CSV\Model\CsvRow;
use Illuminate\Database\Eloquent\Builder;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class CsvService
 *
 * @package CSV\Service
 */
class CsvService
{
    // TODO: replace DI with repository
    /**
     * @var int
     */
    private $int;

    /**
     * CsvService constructor.
     *
     * @param int $int
     */
    public function __construct(
        int $int
    ) {
        $this->int = $int;
    }

    /**
     * @param array $data
     * @return array
     */
    public function process(array $data): array
    {
        $data[] = $this->int;
        return $data;
    }

    /**
     * @param UploadedFile $uploadedFile
     */
    public function processFile(UploadedFile $uploadedFile): void
    {
        $path = $uploadedFile->store('csvs');

        $csvFile = new CsvFile();
        $csvFile->setName($uploadedFile->getClientOriginalName());
        $csvFile->setPath($path);
        $csvFile->save();

        ProcessCsv::dispatch($csvFile);
    }

    public function getRows(
        int $csvFileId,
        ?string $gender = null,
        ?string $category = null,
        ?string $birth_date_lower = null,
        ?string $birth_date_higher = null
    ): Builder
    {
        //should be in a repo, where we inject a service, and repo in a service, but for speed like this.
        $rows = CsvRow::query()
            ->where('csv_file_id', $csvFileId)
            ->orderByDesc('created_at');

        //of course should have been implemented filtering service, but
        if (null !== $gender) {
            $rows->where('gender', $gender);
        }
        if (null !== $category) {
            $rows->where('category', $category);
        }

        //i'm ignoring age filter because it's the same (and exact date filter)
        //not age range but birth date range
        if (null !== $birth_date_lower) {
            $rows->where('birth_timestamp', '>=', Carbon::createFromDate($birth_date_lower)->unix());
        }
        if (null !== $birth_date_higher) {
            $rows->where('birth_timestamp', '<=', Carbon::createFromDate($birth_date_higher)->unix());
        }

        return $rows;
    }
}
