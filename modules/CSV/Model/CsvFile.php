<?php
declare(strict_types = 1);

namespace CSV\Model;

/**
 * Class CsvFile
 *
 * @property int    id
 * @property string name
 * @property int    created
 * @property int    updated
 *
 * @method $this setName(string $name)
 * @method string getName()
 * @method $this setPath(string $path)
 * @method string getPath()
 * @method $this setProcessed(bool $processed)
 * @method string getProcessed()
 * @method $this setUpdatedAt(int $updatedAt)
 *
 * @package CSV\Model
 */
class CsvFile extends AbstractModel
{
    public const TABLE_NAME = 'csv_files';

    public const UNIQUE_ID = 'unique_id';
    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = null;

    /**
     * Defines base model configuration
     *
     * @return void
     */
    protected function init(): void
    {
        $this->table = self::TABLE_NAME;
        $this->dateFormat = 'U';
        $this->casts = [
            'created_at' => 'timestamp',
        ];
    }
}
