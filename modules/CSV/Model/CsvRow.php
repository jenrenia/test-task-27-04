<?php
declare(strict_types = 1);

namespace CSV\Model;

/**
 * Class CsvRow
 *
 * @property int    id
 * @property string name
 * @property int    created
 * @property int    updated
 *
 * @method $this setCategory(string $name)
 * @method string getCategory()
 * @method $this setFirstName(string $firstName)
 * @method string getFirstName()
 * @method $this setLastName(string $lastName)
 * @method string getLastName()
 * @method $this setEmail(string $email)
 * @method string getEmail()
 * @method $this setGender(string $gender)
 * @method string getGender()
 * @method $this setBirthTimestamp(int $birthTimeStamp)
 * @method string getBirthTimestamp()
 * @method $this setCsvFileId(int $csvFileId)
 * @method string getCsvFileId()
 *
 * @package CSV\Model
 */
class CsvRow extends AbstractModel
{
    public const TABLE_NAME = 'csv_rows';

    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = null;

    /**
     * Defines base model configuration
     *
     * @return void
     */
    protected function init(): void
    {
        $this->table = self::TABLE_NAME;
        $this->dateFormat = 'U';
        $this->casts = [
            'created_at' => 'timestamp',
        ];
    }
}
